#pragma once


#define _Punkt2D_H_
#include <iostream>
using namespace std;


class Punkt2D {
private:
	double X;
	double Y;
public:
	Punkt2D();
	Punkt2D(double);
	Punkt2D(double, double);

	const Punkt2D operator+(const Punkt2D &) const;
	const Punkt2D operator-(const Punkt2D &) const;
	const Punkt2D operator/(const double a) const;
	const Punkt2D operator*(const double a) const;

	friend Punkt2D& operator+=(Punkt2D &left, const Punkt2D &right);
	friend Punkt2D& operator-=(Punkt2D &left, const Punkt2D &right);
	friend Punkt2D& operator*=(Punkt2D &left, const double a);
	friend Punkt2D& operator/=(Punkt2D &left, const double a);

	friend bool operator==(const Punkt2D &left, const Punkt2D &right);

	friend ostream& operator<<(ostream& w, const Punkt2D& s);
	friend istream& operator>>(istream& w, Punkt2D& s);
	

	void distance();
	~Punkt2D();
	void wypisz();



};

