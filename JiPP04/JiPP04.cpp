
//JiPP03. SmartPointer ze zliczaniem wystapien

#include "stdafx.h"
#include <iostream>


using namespace std;

class Team
{
	int number;
	char* name;

public:
	Team() : name(0), number(0){}
	Team(char* name, int number) : name(name), number(number){}
	~Team(){}

	void Display()
	{
		cout << "Nazwa zespolu " << name << ", numer zespolu " << number << endl;
	}
};


class RefCount
{
private:
	int count; 

public:
	void Add()
	{
		count++;
	}

	int Release()
	{
		//Zwraca ilosc wystapien
		return --count;
	}
};

template < typename T > class SmartPtr
{
private:
	T*   ptr; 
	RefCount* reference;

public:
	SmartPtr() : ptr(0), reference(0)
	{
		reference = new RefCount();
		// Inkrementuje ilosc wystapien
		reference->Add();
	}

	SmartPtr(T* pValue) : ptr(pValue), reference(0)
	{
		reference = new RefCount();
		reference->Add();
	}

	SmartPtr(const SmartPtr<T>& smrtptr) : ptr(smrtptr.ptr), reference(smrtptr.reference)
	{
		reference->Add();
	}

	~SmartPtr()
	{
		// Destruktor
		// Dekrementuje ilosc wystapien
		// jezeli jest rowna 0, to usuwa dane
		if (reference->Release() == 0)
		{
			delete ptr;
			delete reference;
		}
	}

	T& operator* () { return *ptr; }

	T* operator-> () { return ptr; }

	SmartPtr<T>& operator = (const SmartPtr<T>& smrtptr)
	{
		// Operator przypisania
		if (this != &smrtptr) 
		{
			// Dekrementuje stary licznik wystapien
			// jezeli jest rowny 0, usuwa stare dane
			if (reference->Release() == 0)
			{
				delete ptr;
				delete reference;
			}

			// Kopiuje dane i ilosc wystapien i inkrementuje ilosc wystapien
			pData = smrtptr.ptr;
			reference = smrtptr.reference;
			reference->Add();
		}
		return *this;
	}
};

void main()
{

	SmartPtr<Team> p(new Team("Zespol", 30));
	p->Display();
	
	SmartPtr<Team> q = p;
	q->Display();
}

