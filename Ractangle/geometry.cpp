#include "geometry.h"

Ractangle::Ractangle() : x(0), y(0), szerokosc(0), wysokosc(0) {
}

Ractangle::Ractangle(double x, double y, double szerokosc, double wysokosc) : x(x), y(y), szerokosc(szerokosc), wysokosc(wysokosc) {

}


double Ractangle::getX() {
	return x;
}
double Ractangle::getY() {
	return y;
}
double Ractangle::getSzerokosc() {
	return szerokosc;
}
double Ractangle::getWysokosc() {
	return wysokosc;
}



void Ractangle::setX(double Ox) {
	x = Ox;
}
void Ractangle::setY(double Oy) {
	y = Oy;
}
void Ractangle::setSzerokosc(double s) {
	szerokosc = s;
}
void Ractangle::setWysokosc(double w) {
	wysokosc = w;
}

ostream& operator<<(ostream& w, const Ractangle& s)
{
	w << "(" << s.x << "," << s.y << "," << s.szerokosc << "," << s.wysokosc << ")" << endl;
	return w;
}

istream& operator>>(istream &w, Ractangle & s)
{
	w >> s.x >> s.y >> s.szerokosc >> s.wysokosc;
	return w;
}



double Ractangle::koordKoncowa(double k, double dlugosc) {
	double Kk;
	Kk = k + dlugosc;
	return Kk;
}

Ractangle::~Ractangle() {

}



