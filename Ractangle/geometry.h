#ifndef GEOMETRY_H
#define GEOMETRY_H
#include <iostream>
using namespace std;

class Ractangle {
private:
	double x;
	double y;
	double szerokosc;
	double wysokosc;

public:
	Ractangle();
	Ractangle(double,double,double,double);
	
	double getX();
	double getY();
	double getSzerokosc();
	double getWysokosc();
	
	void setX(double);
	void setY(double);
	void setSzerokosc(double);
	void setWysokosc(double);


	friend ostream& operator<<(ostream& w, const Ractangle& s);
	friend istream& operator>>(istream& w, Ractangle& s);

	double koordKoncowa(double, double);

	~Ractangle();

};


#endif //GEOMETRY_H

