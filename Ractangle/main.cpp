
#include "geometry.h"

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

void ObliczCzescWspolna(Ractangle r1, Ractangle r2){
	double x1, xk1, y1, yk1, s1, w1;
	double x2, xk2, y2, yk2, s2, w2;
	
	double x3, xk3, y3 , yk3, s3, w3;
	
	
	x1 = r1.getX();
	x2 = r2.getX();
	
	y1 = r1.getY();
	y2 = r2.getY();
	
	s1 = r1.getSzerokosc();
	s2 = r2.getSzerokosc();
	
	w1 = r1.getWysokosc();
	w2 = r2.getWysokosc();
	
	xk1 = r1.koordKoncowa(x1, s1);
	xk2 = r2.koordKoncowa(x1, s1);
	
	yk1 = r1.koordKoncowa(y2, w2);
	yk2 = r2.koordKoncowa(y2, w2);
	
	if(x1 > xk2 && x2 > xk1 || y1 > yk2 && y2 > yk1){
		Ractangle r3(0,0,0,0);
		cout << r3;
		system("pause");
	}
	
	if(x1 > x2){
		x3 = x1;
	}
	else x3 = x2;
	
	if(xk1 > xk2){
		xk3 = xk2;
	}
	else xk3 = xk1;
	
	if(y1 > y2){
		y3 = y1;
	}
	else y3 = y2;
	
	if(yk1 > yk2){
		yk3 = yk2;
	}
	else yk3 = yk1;
	
	s3 = xk3 - x3;
	w3 = yk3 - y3;
	
	Ractangle r3(x3, y3, s3, w3);
	cout << r3;
		
}


void CzescWspolna(Ractangle r1, Ractangle r2){
	Ractangle r3;
	
	double xk3, yk3;
		
	
	if(r1.getX() > r2.koordKoncowa(r2.getX(), r2.getSzerokosc()) && r2.getX() > r1.koordKoncowa(r1.getX(), r1.getSzerokosc()) || r1.getY() > r2.koordKoncowa(r2.getY(), r2.getWysokosc()) && r2.getY() > r1.koordKoncowa(r1.getY(), r1.getWysokosc()))
	{
		
		r3.setX(0);
		r3.setY(0);
		r3.setSzerokosc(0);
		r3.setWysokosc(0);
		
		
		cout << r3;
		system("pause");
	}
	
	if(r1.getX() > r2.getX()){
		r3.setX(r1.getX());
	}
	else r3.setX(r2.getX());
	
	if(r1.koordKoncowa(r1.getX(),r1.getSzerokosc()) > r2.koordKoncowa(r2.getX(),r2.getSzerokosc())){
		xk3 = r2.koordKoncowa(r2.getX(), r2.getSzerokosc());
		
	}
	else xk3 = r1.koordKoncowa(r1.getX(), r1.getSzerokosc());
	
	if(r1.getY() > r2.getY()){
		r3.setY(r1.getY());
	}
	else r3.setY(r2.getY());
	
	if(r1.koordKoncowa(r1.getY(), r1.getWysokosc()) > r2.koordKoncowa(r2.getY(),r2.getWysokosc())){
		yk3 = r2.koordKoncowa(r2.getY(),r2.getWysokosc());
	}
	else yk3 = r1.koordKoncowa(r1.getY(),r1.getWysokosc());
	
	r3.setSzerokosc(xk3 - r3.getX());
	r3.setWysokosc(yk3 - r3.getY());

	cout << r3;
}

int main(int argc, char** argv) {
	Ractangle r1, r2;
	cout << "Wpisz po kolei parametry 1 prostokata:" << endl;
	cin >> r1;
	cout << "Wpisz po kolei parametry 2 prostokata:" << endl;
	cin >> r2;
	
	CzescWspolna(r1, r2);
	
	cout << r1;
	cout << r2;
	
	return 0;
}
