#include <cstdlib>
#include <cstring>

#include "Settings.h"

using namespace std;

Settings::Settings() {
	connectionString = new char[260];
	configPathString = new char[260];

	connectionString = '\0';
	configPathString = '\0';
}

Settings::~Settings() {
	if (connectionString != NULL) delete [] connectionString;
	if (configPathString != NULL) delete [] configPathString;
}

Settings& Settings::getInstance() {
	static Settings instance;
	counter++;
	return instance;
}

void Settings::UstawPolaczenieZBazaDanych(const char* connectionString) {
	this->connectionString = connectionString;
}

void Settings::UstawPlikKonfiguracyjny(const char* configPathString) {
	char check[11] = "config.cfg";
	int len = strlen(configPathString);
	int i = len - 10,
		j = 0;

	if (len >= 10) {
		while (configPathString[i]) {
			if (configPathString[i++] == check[j++]) {
			} else break;
		}

		if (j == 10) {
			this->configPathString = configPathString;
		}
	}
}
