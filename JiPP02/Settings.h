#ifndef _Settings_H_
#define _Settings_H_

class Settings {
	private:
		Settings();

		static unsigned int counter;
		
		const char *connectionString;
		const char *configPathString;

		Settings(Settings const&);
		void operator=(Settings const&);
	public:
		~Settings();

		static Settings& getInstance();
		void UstawPolaczenieZBazaDanych(const char*);
		void UstawPlikKonfiguracyjny(const char*);
};

#endif _Settings_H_
