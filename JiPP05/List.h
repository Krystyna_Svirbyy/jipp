#pragma once
template<typename T>
class List {
private:
	struct Node {
		Node(const T& value) : _value(value) { }
		T _value;
		Node* next = nullptr;
		Node* previous = nullptr;
	};

	Node* m_front = nullptr;
	Node* m_back = nullptr;

	void insertBefore(const T& element, Node* node);
	void insertAfter(const T& element, Node* node);
	void insertFront(const T& element);
	void insertBack(const T& element);
	void remove(Node* node);

public:
	List();
	bool empty();
	int size();
	T& front();
	T& back();
	
	void push_back(const T& element);
	void push_front(const T& element);
	void pop_front();
	void pop_back();
	void insert(int position, T& element);
	void printAll();

	~List();
};