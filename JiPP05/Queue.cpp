#include "stdafx.h"
#include<stdlib.h>
#include "Queue.h"


template<typename T>
Queue<T>::Queue() :size(0) {}

template<typename T>
void Queue<T>::push_back(T x) {
	tab[size] = x;
	size++;
}

template<typename T>
T Queue<T>::pop_front() {
	for (int i = 0; i < size - 1; i++) {
		tab[i] = tab[i + 1];
	}
	size--;
	return tab[0];
}
template<typename T>
T Queue<T>::front() {
	return tab[0];
}

template<typename T>
T Queue<T>::back() {
	return tab[size - 1];
}

template<typename T>
int Queue<T>::size_q() {
	if (size <= 0) return 0;
	else return size - 1;
}

template<typename T>
bool Queue<T>::empty() const{
if (size == 0) 	return true;
else return false;
}



