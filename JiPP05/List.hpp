#pragma once
#include "List.h"

template<typename T>
List<T>::List() {

}

template<typename T>
bool List<T>::empty() {
	return m_front != nullptr;
}

template<typename T>
int List<T>::size() {
	int size = 0;
	Node* current = m_front;
	do {
		size++;
	} while ((current = current->next) != nullptr);
	return size;
}

template<typename T>
T& List<T>::front() {
	if (m_front != nullptr) {
		return m_front->_value;
	}
	else {
		return nullptr;
	}
}

template<typename T>
T& List<T>::back() {
	if (m_back != nullptr) {
		return m_back->_value;
	}
	else {
		return nullptr;
	}
}

template<typename T>
void List<T>::insertBefore(const T& element, Node* node) {
	Node* newNode = new Node(element);

	newNode->next = node;
	if (node->previous == nullptr) {
		newNode->previous = nullptr;
		m_front = newNode;
	}
	else {
		newNode->previous = node->previous;
		node->previous->next = newNode;
	}
	node->previous = newNode;
}


template<typename T>
void List<T>::insertAfter(const T& element, Node* node) {
	Node* newNode = new Node(element);
	newNode->previous = node;
	if (node->next == nullptr) {
		newNode->next = nullptr;
		m_back = newNode;
	}
	else {
		newNode->next = node->next;
		node->next->previous = newNode;
	}
	node->next = newNode;
}


template<typename T>
void List<T>::insertFront(const T& element) {
	if (m_front == nullptr) {
		Node* newNode = new Node(element);
		m_front = newNode;
		m_back = newNode;
	}
	else {
		insertBefore(element, m_front);
	}
}

template<typename T>
void List<T>::insertBack(const T& element) {
	if (m_back == nullptr) {
		insertFront(element);
	}
	else {
		insertAfter(element, m_back);
	}
}

template<typename T>
void List<T>::remove(Node* node) {
	if (node->previous == nullptr) {
		m_front = node->next;
	}
	else {
		node->previous->next = node->next;
	}

	if (node->next == nullptr) {
		m_back = node->previous;
	}
	else {
		node->next->previous = node->previous;
	}

	delete node;
}

template<typename T>
void List<T>::push_back(const T& element) {
	insertBack(element);
}

template<typename T>
void List<T>::push_front(const T& element) {
	insertFront(element);
}

template<typename T>
void List<T>::pop_front() {
	remove(m_front);
}

template<typename T>
void List<T>::pop_back() {
	remove(m_back);
}

template<typename T>
void List<T>::insert(int position, T& element) {
	Node* nodeInPosition = m_front;
	int counter = 1;
	while (nodeInPosition != nullptr && counter != position) {
		counter++;
		nodeInPosition = nodeInPosition->next;
	}

	insertAfter(element, nodeInPosition);
}

template<typename T>
void List<T>::printAll() {
	Node* current = m_front;
	do {
		std::cout << current->_value << " ";
	} while ((current = current->next) != nullptr);
}

template<typename T>
List<T>::~List() {
	Node* current = m_front;
	do {
		Node* next = current->next;
		delete current;
		current = next;
	} while (current != nullptr);

}