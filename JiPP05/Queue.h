#pragma once
#define _Queue_H_
#include <iostream>
using namespace std;


template<typename T> class Queue {
private:
	T tab[100];
	int size;
public:
	Queue();
	void push_back(T x);
	T pop_front();
	T front();
	T back();
	int size_q();
	bool empty() const;

};
