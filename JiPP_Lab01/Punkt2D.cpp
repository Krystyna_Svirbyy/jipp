#include "stdafx.h"
#include "Punkt2D.h"



Punkt2D::Punkt2D() :X(0), Y(0) {
}

Punkt2D::Punkt2D(double X) : X(X), Y(0) {
}

Punkt2D::Punkt2D(double X, double Y) : X(X), Y(Y) {
}

const Punkt2D Punkt2D::operator+(const Punkt2D &p) const {
	return Punkt2D(X + p.X, Y + p.Y);
}

const Punkt2D Punkt2D::operator-(const Punkt2D &p) const {
	return Punkt2D(X - p.X, Y - p.Y);
}


const Punkt2D Punkt2D::operator/(const double a) const {
	return Punkt2D(X /a, Y/a);
}

const Punkt2D Punkt2D::operator*(const double a) const {
	return Punkt2D(X * a, Y * a);
}

Punkt2D& operator+=(Punkt2D& left, const Punkt2D& right) {
	left.X += right.X;
	left.Y += right.Y;
	return left;

}

Punkt2D& operator-=(Punkt2D& left, const Punkt2D& right) {
	left.X -= right.X;
	left.Y -= right.Y;
	return left;

}


Punkt2D& operator*=(Punkt2D& left, const double a) {
	left.X *= a;
	left.Y *= a;
	return left;

}

Punkt2D& operator/=(Punkt2D& left, const double a) {
	left.X /= a;
	left.Y /= a;
	return left;

}

bool operator==(const Punkt2D& left, const Punkt2D& right) {
	return (left.X == right.X && left.Y == right.Y);
}


ostream& operator<<(ostream& w, const Punkt2D& s)
{
	w << "(" << s.X << "," << s.Y << ")" << endl;
	return w;
}

istream& operator>>(istream &w, Punkt2D & s)
{
	w >> s.X >> s.Y;
	return w;
}



void Punkt2D::distance(){
	double odleglosc = 0;
	odleglosc = pow(pow(X, 2) + pow(Y, 2), 1. / 2);
	cout << "Odleglosc punktu od (0,0) rowna " << odleglosc << endl;
}


Punkt2D::~Punkt2D(){
}

void Punkt2D::wypisz() {
	cout << "p = " << "(" << X << "," << Y << ");" << endl;
}